import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { PersonAddComponent } from "./person-add/person-add.component";
import { PersonViewComponent } from "./person-view/person-view.component";

@NgModule({
  declarations: [AppComponent, PersonAddComponent, PersonViewComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
