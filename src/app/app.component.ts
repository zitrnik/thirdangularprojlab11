import { Component, OnInit, OnDestroy } from "@angular/core";
import Person from "./shared/models/person.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  title = "Компоненты";
  persons: Person[] = [];
  constructor() {}
  ngOnInit(): void {
    for (let index = 0; index < 5; index++) {
      this.persons.push(new Person("Ivan", "Ivanov", index));
    }
  }
  ngOnDestroy(): void {}
  onAddPerson(person) {
    let newId = +this.persons[this.persons.length - 1].id + 1;
    person.id = newId;
    this.persons.push(person);
  }
  onEditPerson(person) {
    this.persons.splice(
      this.persons.findIndex(elem => elem.id == person.id),
      1,
      person
    );
  }
  deleteFromArr(id) {
    this.persons.splice(this.persons.findIndex(elem => elem.id == id), 1);
  }
}
