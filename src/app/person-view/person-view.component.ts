import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import Person from "../shared/models/person.model";

@Component({
  selector: "app-person-view",
  templateUrl: "./person-view.component.html",
  styleUrls: ["./person-view.component.css"]
})
export class PersonViewComponent implements OnInit {
  @Input() person: Person;
  @Output() delete = new EventEmitter<number>();
  @Output() editPerson = new EventEmitter<Person>();
  constructor() {}
  onDelete(id: number) {
    this.delete.emit(id);
  }
  nowEdit = false;
  onEdit(personId, firstNameData, lastNameData, btn) {
    if (this.nowEdit) {
      firstNameData.innerHTML = firstNameData.childNodes[0].value;
      lastNameData.innerHTML = lastNameData.childNodes[0].value;
      btn.innerHTML = "Редактировать";
      let editedPerson = new Person(
        firstNameData.innerHTML,
        lastNameData.innerHTML,
        personId
      );
      this.editPerson.emit(editedPerson);
    } else {
      firstNameData.innerHTML = `<input class="form-control" type="text" placeholder="Имя" name="" value="${firstNameData.innerHTML.trim()}">`;
      lastNameData.innerHTML = `<input class="form-control" type="text" placeholder="Фамилия" name="" value="${lastNameData.innerHTML.trim()}">`;
      btn.innerHTML = "Сохранить";
    }
    this.nowEdit = !this.nowEdit;
  }
  ngOnInit() {}
}
