import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import Person from "../shared/models/person.model";

@Component({
  selector: "app-person-add",
  templateUrl: "./person-add.component.html",
  styleUrls: ["./person-add.component.css"]
})
export class PersonAddComponent implements OnInit {
  @Output() addPerson = new EventEmitter<Person>();
  constructor() {}

  ngOnInit() {}
  onAddPerson(firstNameInput, lastNameInput, btn) {
    let person = new Person(firstNameInput.value, lastNameInput.value);
    firstNameInput.value = "";
    lastNameInput.value = "";
    this.addPerson.emit(person);
    btn.disabled = true;
  }
  dis = false;
  onValidate(firstNameInput, lastNameInput, btn) {
    if (firstNameInput.value && lastNameInput.value) {
      btn.disabled = false;
    }
  }
}
